By default, Drupal allows only users with the
administer nodes and view/reset/delete revision
permission to add, reset and delete
node revisions of all content type.

Node revision Permissions Per Role allows to give
roles administer per content type
node revision permissions without giving them
full administer nodes permission.

For instance, you may let certain users(eg: roles:External-blogger )
manage node revision of a specific content type(eg: blog)
but restrict to manage node revision of
all other content type that
this module is perfectly works for you.

Installation
Install the module like any other module.
Navigate to admin/people/permissions and set permissions
for each role and each content type
under Node revision Permissions Per Role.

Recommendations
If you do not see the permissions under permission page,
please clear all cache of the site.
After doing some modification of permission on permission page
it is recommended to rebuild the permission.